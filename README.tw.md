# KiCadOpenSourceProjectCollection

#### 介紹

KiCad 開源專案合集

#### 軟體架構

本倉庫使用 `git submodule` 方式收集 KiCad 開源專案合集

#### 安裝教程

1. 克隆本倉庫

```bash
git clone https://gitee.com/KiCAD-CN/KiCadOpenSourceProjectCollection.git
```

2. 使用子模組倉庫

```bash
// 初始化子模組倉庫
git submodule init 

// 更新子模組倉庫
git submodule update
```

或：

```bash
// 初始化和更新子模組倉庫組合命令
git submodule update --init --recursive
```

### KiCad 官網收集[使用 KiCad 設計](https://www.kicad.org/made-with-kicad/) 的工程

這些是一些由使用者製作的 KiCad 設計的專案。

如果您想與本頁上的其他展示案例一起展示，請在 [GitLab 上](https://gitlab.com/kicad/services/kicad-website) 提交合並請求 (如果您符合新增專案要求的貢獻指南)！

#### 使用說明

1. [dvk-mx8m-bsb](dvk-mx8m-bsb) 開源的智慧手機，倉庫地址：https://source.puri.sm/Librem5/dvk-mx8m-bsb


> # dvk-mx8m-bsb
> 
> Librem 5 Developer Kit using EmCraft's i.MX 8M System-on-Module.
> 
> KiCad v5.0.0 has been used to create this design.
> 
> ## License
> 
> dvk-mx8m-bsb is [licensed under the GNU GPLv3+](https://source.puri.sm/Librem5/dvk-mx8m-bsb/blob/master/LICENSE).


2. [usbarmory](usbarmory) 倉庫地址：https://github.com/f-secure-foundry/usbarmory/

> # usbarmory
> Introduction
> ============
> 
> USB armory | https://github.com/f-secure-foundry/usbarmory  
> Copyright (c) F-Secure Corporation
> 
> The USB armory from [F-Secure Foundry](https://foundry.f-secure.com) is an open
> source hardware design, implementing a flash drive sized computer.
> 
> This repository is aimed towards developers, if you wish to purchase a USB
> armory board please see the [USB armory project page](https://www.f-secure.com/en/consulting/foundry/usb-armory).
> 
> Authors
> =======
> 
> Andrea Barisani  
> andrea.barisani@f-secure.com | andrea@inversepath.com  
> 
> Andrej Rosano  
> andrej.rosano@f-secure.com   | andrej@inversepath.com  
> 
> Daniele Bianco  
> daniele.bianco@f-secure.com   | daniele@inversepath.com  
> 
> Documentation
> =============
> 
> The main documentation can be found on the
> [project wiki](https://github.com/f-secure-foundry/usbarmory/wiki).
> 
> Board revisions
> ===============
> 
> * [USB armory Mk II](https://github.com/f-secure-foundry/usbarmory/wiki/Mk-II-Introduction)
> 
> ![Mk II Top](https://github.com/f-secure-foundry/usbarmory/wiki/images/armory-mark-two-top.png)
> ![Mk II Bottom](https://github.com/f-secure-foundry/usbarmory/wiki/images/armory-mark-two-bottom.png)
> 
> * USB armory Mk I: first production release.
> 
> ![USB armory Mk I](https://github.com/f-secure-foundry/usbarmory/wiki/images/armory-mark-one.png)
> 
> License
> =======
> 
> USB armory | https://github.com/f-secure-foundry/usbarmory  
> Copyright (c) F-Secure Corporation
> 
> This is an open hardware design licensed under the terms of the CERN Open
> Hardware Licence (OHL) v1.2.
> 
> You may redistribute and modify this documentation under the terms of the CERN
> OHL v.1.2 (http://ohwr.org/cernohl). This documentation is distributed WITHOUT
> ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY
> QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN OHL v.1.2 for
> applicable conditions.


3. [H7PI](H7PI) 倉庫地址：https://gitee.com/htctek/H7PI
> # H7PI
> 簡介
> ============
> H7PI基於STM32H750VBT6, 設計初衷是為了做一個通用平臺，在這個設計上，按照一個場景一個
> 應用的模式擴展出其他擴充套件功能，同時實現如程式碼複用一般的硬體複用。板載8M QSPI Flash程式空間，8M SPI Flasn檔案空間，支援擴充套件SD卡，支援擴充套件LCD.[參考說明](https://pinno.cc/2020/02/28/H7PI/)
> 
> #### KiCad v5.1.5 has been used to create this design.    
> ![H7PI](https://pinno.cc/2020/02/28/H7PI/wp.jpg)
> 
> license
> ============
> 
> H7PI is [licensed under the BSD 3-Clause](https://gitee.com/htctek/H7PI/blob/master/LICENSE).


4. [ESP32PI](ESP32PI) 倉庫地址：https://gitee.com/htctek/ESP32PI
> # H7PI
> 簡介
> ============
> ESP32PI基於ESP32-PICO-D4, 為快速開發wifi，ble應用提供方案驗證的可能
> * 板載CP2102，可透過USB轉串列埠直接更新韌體，無需手動按按鍵
> * 板載Micro SD Card介面，擴充套件檔案系統
> * 板載天線，可直接連結WiFi和藍芽，無需額外購買天線
> * 板載PSRAM，增加應用記憶體空間
> * 所有IO引出，方便複用，可擴充套件多種功能
> [參考說明](https://pinno.cc/2020/04/07/ESP32PI/)
> 
> #### KiCad v5.1.5 has been used to create this design.    
> ![ESP32PI](https://pinno.cc/2020/04/07/ESP32PI/ESP32PI.JPG)
> 
> license
> ============
> 
> ESP32PI is [licensed under the BSD 3-Clause](https://gitee.com/htctek/ESP32PI/blob/master/LICENSE).

5. [reform](reform) 開源的膝上型電腦，倉庫地址：https://source.mntmn.com/MNT/reform

> # MNT Reform
> 
> This is the main repository for the MNT Reform open source laptop.
> 
> For details, check out [Finishing Reform](https://mntre.com/media/reform_md/2020-01-18-finishing-reform.html) and related articles.
> 
> ## Impressions
> 
> ![Reform Version 2 Prototype](https://mntre.com/media/reform_v2_images/reform_v2_prototype_top.jpg)
> ![Reform Version 2 Motherboard](https://mntre.com/media/reform_v2_images/reform_v2_motherboard.jpg)
> 
> ## License
> 
> Copyright 2018-2020 MNT Research GmbH.
> 
> The following licenses are used in the project, unless specified differently in a particular subfolder:
> 
> - Schematics, PCBs: [CERN-OHL-S v2](https://www.ohwr.org/project/cernohl/wikis/uploads/002d0b7d5066e6b3829168730237bddb/cern_ohl_s_v2.txt)
> - Other documentation, artwork, photos and drawings that are not trademarks (see below): [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode)
> - Software, firmware: Various. [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) if not specified otherwise in the file/subdirectory.
> 
> The "MNT" and "MNT REFORM" logos are trademarks of MNT Research GmbH. You may not use these in derived works. The reason for this is that we cannot be responsible for regulatory issues with derived boards and we cannot support them. If someone sees an MNT brand on a product, it has to be clear that it comes from MNT Research and not from a third party.
> 
> ## Made With
> 
> * [KiCAD EDA](http://kicad-pcb.org/)
> * [OpenSCAD](https://www.openscad.org/)
> * [LUFA](http://www.fourwalledcubicle.com/LUFA.php)
> * Autodesk Fusion (Case parts)
> 
> ## MNT Reform Team
> 
> * **Lukas F. Hartmann (mntmn)** - *Schematics, PCB*
> * **Ana Dantas** - *Industrial Design*
> * **Greta Melnik** - *Quality & Assembly, Sleeve*
> 
> ## Credits and Contributions
> 
> * [fully automated technologies](https://fully.automated.ee/) - *inrush limiter circuit*

6. [hs-probe](hs-probe) 開源的高速探針偵錯程式，倉庫地址：https://github.com/probe-rs/hs-probe 

> # The Rusty High-speed Probe
> 
> This is an open hardware probe for the Serial Wire Debug (SWD) and JTAG protocol. It utilizes an
> STM32F723 MCU which has an USB 2.0 High-speed Phy.
> 
> ## Firmware
> 
> The firmware is available here and is open-source: https://github.com/probe-rs/hs-probe-firmware
> 
> ## Features
> 
> There is an USB MUX on the board, so when the probe starts it enumerates in USB FS where one can
> access the internal bootloader, and then when the firmware takes over it can switch to the USB HS.
> 
> * The output connector can either be:
>     * The STDC14 connector used on STLink/V3 which is mounted by default.
>     * Or the standard Cortex-M Debug (1.27mm, 10 pin) connector if one bends/removes the outer 2 pins on the connector.
> * The programming connector is the Tag Connect TC2030, however for just loading firmware the USB bootloader is recommended.
> * USB MUX between USB HS/FS.
> * Castellated vias for all programming pins, power and 2 GPIOs.
> * USB-C connector.
> * Target 5V and 3.3V is protected with an ideal diode circuit to not have current flow from the target to the host.
> * For compatibility (mainly with Tag-Connect TC2050), pin 7 on the STDC 14 can be connected to the protected 5V.
> 
> The schematic can be found in [the schematic PDF](schematic-v1.3.pdf).
> 
> ![alt text](https://github.com/probe-rs/hs-probe/raw/master/hs-probe-top.jpg "probe")
> ![alt text](https://github.com/probe-rs/hs-probe/raw/master/hs-probe-bot.jpg "probe")
> 
> ## License
> 
> This work is licensed under [CERN-OHL-P](cern_ohl_p_v2.txt).

7. [launch](launch) 開源的可換鍵軸的機械鍵盤，倉庫地址：https://github.com/system76/launch

> # System76 Launch Configurable Keyboard
> 
> The System76 Launch Configurable Keyboard is designed to provide the ultimate
> user controlled keyboard experience, with open source mechanical and electrical
> design, open source firmware and associated software, and a large number of
> user configuration opportunities.
> 
> - [Mechanical Design](#mechanical-design)
> - [Electrical Design](#electrical-design)
> - [Firmware and Software](#firmware-and-software)
> 
> ## Mechanical Design
> 
> ![Chassis Image](./chassis/launch-chassis.png)
> 
> ### Open Source Chassis
> 
> The Launch chassis is licensed CC-BY-SA-4.0 and can be viewed in the
> [chassis](./chassis/) folder using [FreeCAD](https://www.freecadweb.org/).
> 
> ### Milled Aluminum
> 
> The chassis is milled from two solid blocks of aluminum and powder coated to
> provide excellent fit and finish. Each pocket, port, and hole is designed and
> precisely machined so that swapping switches and plugging in cables is easy and
> secure for the user.
> 
> ### Detachable Lift Bar
> 
> The included lift bar can be magnetically secured to add 15 degrees of angle to
> your keyboard for ergonomics.
> 
> ### Innovative Layout
> 
> The layout is designed to provide a large number of remapping opportunities.
> The default layout can be viewed
> [here](http://www.keyboard-layout-editor.com/#/gists/8ec5e9026d616ebad6b2c7e9d943e7c0),
> and the extra keys included can be viewed
> [here](http://www.keyboard-layout-editor.com/#/gists/a3ad8710b27f78fd938077b2bf6d3ef5).
> 
> ### Swappable Keycaps
> 
> The keycaps are PBT material with a dye sublimation legend and XDA profile to
> provide excellent feel and lifespan. Extras are provided for common replacements
> and color preference. An included keycap puller can be used to move and replace
> the keycaps.
> 
> ### Swappable Switches
> 
> The switches are mounted in sockets that support any RGB switch with an MX
> compatible footprint. Examples are the Cherry MX RGB switches and the Kailh
> BOX switches. Switches can be removed easily at any time with the included
> switch puller.
> 
> ## Electrical Design
> 
> ![PCB Image](./pcb/launch-pcb.png)
> 
> ### Open Source PCB
> 
> The Launch PCB is licensed GPLv3 and can be viewed in the
> [pcb](./pcb/) folder using [KiCad](https://kicad.org/).
> 
> ### Integrated Dock
> 
> Launch connects to a computer using the included USB-C to USB-C cable or USB-C
> to USB-A cable. It supports USB 3.2 Gen 2 with speeds up to 10 Gbps with either
> cable, provided the computer supports these speeds. It provides 2 USB-C and 2
> USB-A connectors that also support USB 3.2 Gen 2, with the 10 Gbps bandwidth
> shared between them on demand.
> 
> ### Independent RGB Lighting
> 
> Each switch has an RGB LED that is independently controlled by firmware. This
> allows for a number of RGB LED patterns to be selected.
> 
> ### N-Key Rollover
> 
> The keyboard matrix uses diodes on all intersections, providing full independent
> scanning of each key position.
> 
> ## Firmware and Software
> 
> ### Open Source Firmware
> 
> The Launch firmware is based on [QMK](https://github.com/system76/qmk_firmware),
> licensed GPLv2, and the latest version is linked in the `firmware` submodule.
> 
> ### Open Source Software
> 
> Projects that integrate with Launch are open source software, such as the
> [System76 Keyboard Configurator](https://github.com/pop-os/keyboard-configurator),
> licensed GPLv3, and [fwupd](https://github.com/fwupd/fwupd/), licensed LGPLv2.1.
> 
> ### Easy Remapping
> 
> The keyboard can be remapped at runtime using the
> [System76 Keyboard Configurator](https://github.com/pop-os/keyboard-configurator).
> This utility runs on Linux, Mac OS, and Windows.
> 
> ### Firmware Updates
> 
> Firmware updates are supported through the
> [fwupd](https://github.com/fwupd/fwupd/) project, and are distributed using the
> related Linux Vendor Firmware Service. Settings are stored on EEPROM and are
> maintained through firmware updates.


8. [cantact-pro-hw](cantact-pro-hw) 開源 USB 到控制器區域網（CAN）裝置。倉庫地址：https://github.com/linklayer/cantact-pro-hw

> # CANtact Pro User Guide
> 
> ![CANtact Pro](https://github.com/linklayer/cantact-book/blob/master/src/cantact-pro/cantact-pro.jpgg)
> 
> CANtact Pro is [available from CrowdSupply](https://www.crowdsupply.com/linklayer-labs/cantact-pro)
> 
> ## Getting Started
> 
> Thank you for buying a CANtact Pro 🙂
> 
> To start using CANtact, connect it via USB. Before using, you should update to the latest 
> firmware following the instructions in this document.
> 
> After updating, the device will work on Linux via SocketCAN. For details, see [SocketCAN](https://github.com/linklayer/cantact-book/blob/master/src/tools/socketcan.html). 
> 
> On all platforms, you can install the CANtact CLI. For details, see [CANtact CLI](https://github.com/linklayer/cantact-book/blob/master/src/tools/cantact-cli.html).
> 
> For more information on software support, see [Software Tools](https://github.com/linklayer/cantact-book/blob/master/src/tools/software-tools.html).
> 
> ## Features & Specifications
> - 2x CAN Interfaces
>   - 1x CAN/CAN-FD/SWCAN
>   - 1x CAN/CAN-FD
> - High Speed USB interface (480 Mbit/s)
> - Isolation between CAN and USB
> - Industry standard DB9 connectors
> - Software support for Windows, macOS, and Linux
>   - Cross-platform USB driver, command line interface, and APIs:
>     - Python (via [python-can](https://github.com/hardbyte/python-can/))
>     - C / C++
>     - Rust
>   - [ETAS BUSMASTER](https://rbei-etas.github.io/busmaster/) support on Windows
>   - SocketCAN support on Linux
> 
> 
> ## Pin Assignments
> 
> |Pin | Function | CAN 0 Only? |
> |----|----------|-------------|
> |1   |          |
> |2   | CAN -    |
> |3   | GND      |
> |4   | SWCAN    | Yes
> |5   |          |
> |6   | GND      |
> |7   | CAN +    |
> |8   |          |
> |9   | +12 V In | Yes
> 
> Note that single-wire CAN is only available on the CAN 0 connector.
> 
> ## LED Indicators
> 
> The device has four LEDs, two for each CAN channel. When powered on, the LEDs will blink in sequence. The LED indications during normal operations are shown here.
> 
> |Color  | State   | Meaning |
> |-------|---------|---------|
> | Green | Solid   | channel enabled, no activity
> | Green | Blinking| receive activity 
> | Red   | Solid   | channel transmit enabled, no activity
> | Red   | Blinking| transmit activity 
> 
> When using SocketCAN, each CAN interface can be identified by blinking the corresponding LEDs using `ethtool`. This is especially useful when using multiple devices.
> 
> ```
> sudo ethtool --identify can0
> ```
> 
> ## Configuring Permissions on Linux
> 
> By default, root privileges are required to interact with the device. To allow other users to access the device, 
> create a file at `/etc/udev/rules.d/99-cantact.rules`:
> ```
> SUBSYSTEM=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="6070", MODE="0666"
> ```
> 
> After creating the file, reload the udev rules:
> ```
> sudo udevadm control --reload-rules
> sudo udevadm trigger
> ```
> 
> This will make the device accessible to all users. 
> 
> ## Flashing Firmware
> 
> 1. Download the newest firmware binary [from Github](https://github.com/linklayer/cantact-pro-fw/releases).
> 2. To enter bootloader mode, hold down the button beside the USB connector while connecting the device
> to your computer.
> 
> ![Holding the button to enter bootloader mode](https://github.com/linklayer/cantact-book/blob/master/src/cantact-pro/enter-bootloader.jpg)

#### 參與貢獻

1.  Fork 本倉庫

2.  新建 Feat_xxx 分支

3.  新增新的子模組倉庫

```bash
// `URL` 為子模組的路徑，`PATH` 為該子模組儲存的目錄路徑
git submodule add [URL] [PATH]
```
例如：

```bash
// 倉庫字尾的 `.git` 可加可不加
git submodule add https://source.puri.sm/Librem5/dvk-mx8m-bsb
```

4. 從子模組的遠端倉庫更新併合併到本倉庫

```bash
// 從子模組遠端倉庫更新併合並
git submodule update --remote --merge
```
5.  提交子模組的更改

```bash
// 提交修改
// -s : 在提交日誌訊息的末尾新增提交人簽名行。
// 簽約的含義取決於專案，但它通常證明提交者有權在相同的許可證下提交此作品，
// 並同意開發者原產地證書(有關更多資訊，請參閱 http://developercertificate.org/)。

git commit -sa 
```

修改本倉庫的 `README` 自述檔案，增加對新增子模組倉庫的描述。
```bash
// 使用文字編輯器編輯 `README.md`, 如：
// vim notepad gedit notepad++ nano emacs ...
vim README.md
// vim 批次處理 markdown(md) 語法引用操作 >
// 在 vim 編輯介面下，如給 usbarmory 批次新增引用符號 >
// : 編輯模式 54 開始行號 110 結束行號 s 搜尋 ^ 行首 > 替換內容 g 替換
:54,110 s/^/> /g
// 修改完後記得儲存退出，並清理文字編輯的快取檔案。
// 或者將快取檔案的格式新增到 `.gitignore` 檔案中。


// 提交 README.md 修改 
git commit -am "此次修改的描述"
// 例如：
git commit -am "Add dvk-mx8m-bsb"
git commit -am "新增 dvk-mx8m-bsb"
git commit -am "Update dvk-mx8m-bsb"
git commit -am "更新 dvk-mx8m-bsb"
git commit -am "Delete dvk-mx8m-bsb"
git commit -am "刪除 dvk-mx8m-bsb"
```

推送到自己的倉庫
```bash
git push 
```
6.  新建 Pull Request


#### 碼雲特技

1.  使用 Readme\_XXX.md 來支援不同的語言，例如 Readme\_en.md, Readme\_zh.md
2.  碼雲官方部落格 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 這個地址來了解碼雲上的優秀開源專案
4.  [GVP](https://gitee.com/gvp) 全稱是碼雲最有價值開源專案，是碼雲綜合評定出的優秀開源專案
5.  碼雲官方提供的使用手冊 [https://gitee.com/help](https://gitee.com/help)
6.  碼雲封面人物是一檔用來展示碼雲會員風采的欄目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
